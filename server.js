const express = require("express");
const morgan = require("morgan");
const axios = require("axios");
const app = express();
const port = 8080;

app.use(morgan("combined"));

app.get("/", async (req, res) => {
  const randomId = Math.floor(Math.random() * 2119);
  try {
    const {
      data: { img, alt = "" }
    } = await axios.get(`https://xkcd.com/${randomId}/info.0.json`);
    return res.send(`<center><img alt="${alt}" src="${img}"/></center>`);
  } catch (error) {
    res.send(error.response.data);
  }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
